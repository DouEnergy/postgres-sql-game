from dataclasses import dataclass
from typing import  Any, Optional , Type 
    
@dataclass
class Geometric:
    color: str
    left:  Optional[Any] = None  # Type[Geometric]
    right: Optional[Any] = None  # Type[Geometric]
        
@dataclass
class Circle(Geometric):
    pass

@dataclass
class Square(Geometric):
    pass

@dataclass
class Triangle(Geometric):
    pass
    
@dataclass
class Rectangle(Geometric):
    pass

@dataclass
class Apple():
    color: str = 'red'
    left:  Any = None
    right: Any = None
    pass




Geometric_tree1 = Square('green' , 
                    Circle('red' , Triangle('red')),
                    Rectangle('yellow')
                )


Geometric_tree2 = Rectangle('yellow',
                    Square('green' , 
                      Circle('red')
                    )
                  )
                 
Geometric_tree3 = Square('green', 
                    Triangle('yellow' , Triangle('red')),
                    Rectangle('blue')
                  )


Fruit_tree1 = Apple('yellow' , 
                Triangle('yellow' , Triangle('red')),
                Rectangle('blue')
              )


def Find(Tree):
  match Tree:
    case Square('green' , 
            Circle('red')as left , 
            _
          ) as root:
            print(f'case1 \n  {root = } \n {left = } \n ')
      
    case Geometric( _ , 
              Triangle('yellow') as left,
              Rectangle('blue')  as right,
            ):
            print(f'case2 \n {left = } \n {right = } \n')

    case Circle('yellow', 
             _ ,
             Rectangle('blue',
                Triangle('red'),
                Rectangle('red',
                    _ ,
                    Square('green')
                ),
             ) as right
          ) as root:
            print(f'case1 \n {root = } \n {right = } \n ')
            
    case _:
            print('Not Found \n')

print('Geometric_tree1 \n')
Find(Geometric_tree1)
print('-------------------------- \n')

print('Geometric_tree2 \n')
Find(Geometric_tree2)
print('-------------------------- \n')

print('Geometric_tree2 Left Child \n')
Find(Geometric_tree2.left)
print('-------------------------- \n')

print('Geometric_tree3 \n')
Find(Geometric_tree3)
print('-------------------------- \n')

print('Fruit_tree1 \n')
Find(Fruit_tree1)
print('-------------------------- \n')
